"""
This program output the fibonacci number based on the
position of the number given. If the value of the number is
divisible by 3, 5, or 15 it show different output.
If the number is prime, the output will be different too.
For all other case, the output will be the value itself.
"""
from math import sqrt

class OutOfRangeError(ValueError):
    '''Shows out or range errors'''
    pass
class NotIntegerError(ValueError):
    '''Raises error if not an integer'''
    pass

def is_prime(param):
    """Returns True if n is prime."""
    if param == 1:
        return False
    if param == 2:
        return True
    if param == 3:
        return True
    if param % 2 == 0:
        return False
    if param % 3 == 0:
        return False

    root = 5
    divisor = 2

    while root * root <= param:
        if param % root == 0:
            return False

        root += divisor
        divisor = 6 - divisor

    return True

def fib(num):
    """returns nth fibonacci number."""
    if num < 0 or num > 100:
        raise OutOfRangeError("Input out of range, must be between 0 and 100")
    if not isinstance(num, int):
        raise NotIntegerError('non-integers can not be converted')

    res = ((1+sqrt(5))**num-(1-sqrt(5))**num)/(2**num*sqrt(5))
    res = int(res)
    flag = True #if the flag is false default condition will execute
    output = []
    if res%3 == 0:
        output.append('Buzz')
        flag &= False
    if res%5 == 0:
        output.append('Fizz')
        flag &= False
    if res%15 == 0:
        output.append('FizzBuzz')
        flag &= False
    if is_prime(res):
        output.append('BuzzFizz')
        flag &= False
    if flag:
        output.append(res)
    return output


if __name__ == "__main__":
    while True:
        position = raw_input("Insert the nth fibonacci number you want to see: ")
        try:
            position = int(position)
        except ValueError:
            print "Input a positive integer"
            continue
        if position > 100 or position < 0:
            print 'Number too big or small. Choose again: '
        else:
            break
    for i in fib(position):
        print i
