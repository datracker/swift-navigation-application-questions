"""Unit tests for the fibonacci program"""

import unittest
import fibonacci


class KnownValues(unittest.TestCase):
    """Tests with known values"""
    known_val = (
        (0, ['Buzz', 'Fizz', 'FizzBuzz']),
        (1, [1]),
        (2, [1]),
        (3, ['BuzzFizz']),
        (4, ['Buzz', 'BuzzFizz']),
        (5, ['Fizz', 'BuzzFizz']),
        (6, [8]),
        (7, ['BuzzFizz']),
        (8, ['Buzz']),
        (9, [34]),
        (10, ['Fizz']),
        (11, ['BuzzFizz']),
        (12, ['Buzz']),
        (13, ['BuzzFizz']),
        (14, [377]),
        (15, ['Fizz'])
    )

    def test_fibonacci_known_values(self):
        """fibonacci should give known results with known inputs"""
        for integer, output in self.known_val:
            result = fibonacci.fib(integer)
            self.assertEqual(output, result)

class FibonacciBadInput(unittest.TestCase):
    """Tests with bad inputs"""
    def test_too_large(self):
        '''fibonacci should fail with large inputs'''
        self.assertRaises(fibonacci.OutOfRangeError, fibonacci.fib, 125)
    def test_negative(self):
        '''fibonacci should fail with negative input'''
        self.assertRaises(fibonacci.OutOfRangeError, fibonacci.fib, -1)
    def test_non_integer(self):
        '''fibonacci should fail with non-integer input'''
        self.assertRaises(fibonacci.NotIntegerError, fibonacci.fib, 2.5)

if __name__ == '__main__':
    unittest.main()
