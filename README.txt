Programming Problem:

In the programming language of your choice, write a program generating the 
first n Fibonacci numbers F(n), printing

"Buzz" when F(n) is divisible by 3.
"Fizz" when F(n) is divisible by 5.
"FizzBuzz" when F(n) is divisible by 15.
"BuzzFizz" when F(n) is prime.
the value F(n) otherwise.

=================================================================

Solution Overview:

The formula used for fibonacci number:

        (1+sqrt(5))**n - (1-sqrt(5))**n
F(n) = --------------------------------
              sqrt(5) * 2**n



The algorithm for finding the prime number is taken from the AKS primality test.
This is the fastest way to find if a number is prime.
It uses the fact that a prime (except 2 and 3) is of form
6k - 1 or 6k + 1 and looks only at divisors of this form.

--------To run the code--------
*****The code is written on Python 2.7.xx
Go to the directory where the code is saved and in terminal type:
< python fibonacci.py >
You need to have Python 2.xx interpreter to run this program.

--------To run the test file--------
Go to the directory and type "python fibonaccitest.py" in the terminal


-----------Linting-------------
For linting used Pylint 1.7.4.
Go to the directory where the fibonacci file is saved and type in terminal:
< pylint --const-rgx='[a-z_][a-z0-9_]{2,30}$' fibonacci.py >